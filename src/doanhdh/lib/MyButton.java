package doanhdh.lib;

import javax.swing.JButton;

public class MyButton extends JButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public MyButton() {
		
	}
	public MyButton(int TOPX, int TOPY, int WIDTH, int HEIGHT, String name) {
		setBounds(TOPX, TOPY, WIDTH, HEIGHT);
		setText(name);
	}
}
