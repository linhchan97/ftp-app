package doanhdh.lib;

public class TienTrinh {
	private int thoiDiemDen,thoiGianHoanThanh,thoiGianCho,ID,thoiGianDaXuLy;

	public TienTrinh() {}
	public TienTrinh(int ID,int arrivalTime, int burstTime) {
		super();
		this.ID=ID;
		this.thoiDiemDen = arrivalTime;
		this.thoiGianHoanThanh = burstTime;
		this.thoiGianDaXuLy=0;
		this.thoiGianCho=0;
	}

	public int getThoiDiemDen() {
		return thoiDiemDen;
	}

	public void setThoiDiemDen(int thoiDiemDen) {
		this.thoiDiemDen = thoiDiemDen;
	}

	public int getThoiGianHoanThanh() {
		return thoiGianHoanThanh;
	}

	public void setThoiGianHoanThanh(int thoiGianXuLy) {
		this.thoiGianHoanThanh = thoiGianXuLy;
	}

	public int getThoiGianCho() {
		return thoiGianCho;
	}

	public void setThoiGianCho(int thoiGianCho) {
		this.thoiGianCho = thoiGianCho;
	}

	public int getThoiGianDaXuLy() {
		return thoiGianDaXuLy;
	}

	public void setThoiGianDaXuLy(int thoiGianDaXuLy) {
		this.thoiGianDaXuLy = thoiGianDaXuLy;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
}
