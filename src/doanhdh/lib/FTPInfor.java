package doanhdh.lib;

public class FTPInfor {
	private String server;
	private int port;
	private String user;
	private String pass;
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public FTPInfor(String server, int port, String user, String pass) {
		super();
		this.server = server;
		this.port = port;
		this.user = user;
		this.pass = pass;
	}
	@Override
	public String toString() {
		return "FTPInfor [server=" + server + ", port=" + port + ", user=" + user + ", pass=" + pass + "]";
	}
	
	
}
