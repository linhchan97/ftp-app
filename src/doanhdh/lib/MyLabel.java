package doanhdh.lib;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;

public class MyLabel extends JLabel {

	public MyLabel(int width, int height, String cap, int size, Color color) {
		setBounds(10, 10, width, height);
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setForeground(color);
		setFont(new Font("Arial", Font.BOLD, size));
		setText(cap);
	}
}
