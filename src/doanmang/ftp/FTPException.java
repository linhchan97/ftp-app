package doanmang.ftp;

public class FTPException extends Exception {
	public FTPException(String message) {
		super(message);
	}
}