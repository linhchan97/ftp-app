package doanmang.ftp;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.net.ftp.FTPClient;

public class SwingCreateDirectory extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel dirName;
	private JTextField dirNameTf;
	private JButton createBt;
	
	public SwingCreateDirectory(FTPClient ftpClient) {
		this.setTitle("Make new directory FTP server");
		JPanel panel = new JPanel();
		this.add(panel);
		this.setSize(500, 100);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
		dirName = new JLabel("Directory name:");
		dirName.setBounds(5,20,50,50);
		dirNameTf = new JTextField(20);
		createBt = new JButton("Create");
		createBt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int dialogConfirm = JOptionPane.showConfirmDialog(createBt, "Do you want to delete this file?");
				if(JOptionPane.YES_OPTION == dialogConfirm) {
					FTPUtility util = new FTPUtility();
					boolean result = util.createDirectory(dirNameTf.getText(),ftpClient);
					if(result) {
						JOptionPane.showMessageDialog(null, "Created directory successfully!");
						exitFrame();
					}
					
				}
			}
		});
		panel.add(dirName);
		panel.add(dirNameTf);
		panel.add(createBt);
		this.setVisible(true);
	}

	protected void exitFrame() {
		this.setVisible(false);
	}
	
	
}
