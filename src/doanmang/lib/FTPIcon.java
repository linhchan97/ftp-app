package doanmang.lib;

public class FTPIcon {
	private static final String returnIcon = "src\\image\\\\GoRtlHS.png";
	private static final String goToIcon = "src\\image\\\\FormRunHS.png";
	private static final String downloadIcon = "src\\image\\FillDownHS.png";
	private static final String uploadIcon ="src\\image\\FillUpHS.png";
	private static final String newFolderIcon = "src\\image\\NewFolderHS.png";
	private static final String deleteIcon = "src\\image\\DeleteHS.png";
	private static final String refreshIcon = "src\\image\\refresh.png";
	
	public static String getReturnIcon() {
		return returnIcon;
	}

	public static String getGotoicon() {
		return goToIcon;
	}

	public static String getDownloadicon() {
		return downloadIcon;
	}

	public static String getUploadicon() {
		return uploadIcon;
	}

	public static String getNewfoldericon() {
		return newFolderIcon;
	}

	public static String getDeleteicon() {
		return deleteIcon;
	}

	public static String getRefreshicon() {
		return refreshIcon;
	}
	
}
