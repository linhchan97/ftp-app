package doanmang.ftp;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import doanhdh.lib.MyLabel;
import doanhdh.lib.MyTableModel;
import doanhdh.lib.MyTextField;
import doanmang.lib.FTPIcon;

public class MainUI extends JFrame implements MouseListener, TableModelListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FTPUtility util;
	private FTPClient ftpClient;
	private JTable serverResponseTable, remoteTable;
	private DefaultMutableTreeNode root;
	private JTree tree;
	private DefaultTreeModel treeModel;
	private MyTableModel fileModel, responseModel;
	private JTextField toDirTf;
	private JLabel toDirLb, returnDirLb, downloadLb, uploadLb, newfolderLb, deleteFileLb, refreshLb;
	private JPanel top, west, east, bottom, toolbar, tablepane;
	private JScrollPane tablescrool, responseScroll;
	private JTextPane consoleArea;
	private FlowLayout flowLayout;
	private File fileRoot ;
	private JScrollPane treeScroll;

	public MainUI(FTPUtility util, FTPClient ftpClient) {
		util = new FTPUtility();
		this.util = util;
		this.ftpClient = ftpClient;
		prepareGUI();
	}

	private void prepareGUI() {
		this.setTitle("FTP Client - Made by Linh");
		this.setSize(900, 700);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		flowLayout = new FlowLayout();
		this.getContentPane().setLayout(flowLayout);
		

		top = createPanel(880, 100, Color.black, "Console", Color.white);
		addElementToTop(top);

		west = createPanel(440, 400, Color.white, "Remote machine", Color.black);
		west.setLayout(flowLayout);
		toolbar = createPanel(430, 40, Color.white, "", Color.black);
		toolbar.setLayout(flowLayout);
		addElementToToolbar(toolbar);
		west.add(toolbar);

		tablepane = createPanel(430, 350, Color.white, "", Color.black);
		west.add(tablepane);
		addTableModel("/");
		remoteTable = new JTable(fileModel);
		remoteTable.setPreferredScrollableViewportSize(new Dimension(400, 300));
		tablescrool = new JScrollPane(remoteTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		remoteTable.setEnabled(false);
		tablepane.add(tablescrool);

		addTreeToEast();
		east = createPanel(440, 400, Color.white, "Local machine", Color.black);
		treeScroll= new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		treeScroll.setPreferredSize(new Dimension(420, 370));
		east.add(treeScroll);
		
		
		bottom = createPanel(880, 150, Color.white, "Response from server", Color.black);
		responseModel = new MyTableModel();
		addElementToBottom(bottom);
		serverResponseTable = new JTable(responseModel);
		serverResponseTable.setEnabled(false);
		serverResponseTable.setPreferredScrollableViewportSize(new Dimension(860, 140));
		responseScroll = new JScrollPane(serverResponseTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		bottom.add(responseScroll);

		this.add(top);
		this.add(west);
		this.add(east);
		this.add(bottom);
		this.setVisible(true);
	}

	private void addTreeToEast() {
		fileRoot= new File("E:/");
		root = new DefaultMutableTreeNode(new FileNode(fileRoot));
		treeModel = new DefaultTreeModel(root);

		tree = new JTree(treeModel);
		tree.setShowsRootHandles(true);
		CreateChildNodes ccn = new CreateChildNodes(fileRoot, root);
		new Thread(ccn).start();
		
	}

	private void addElementToTop(JPanel top) {
		consoleArea = new JTextPane();
		consoleArea.setBackground(Color.black);
		consoleArea.setPreferredSize(new Dimension(860, 70));
		consoleArea.setEditable(false);
		consoleArea.setForeground(Color.red);
		try {
			consoleArea.setText(ftpClient.getStatus());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JScrollPane topSrcoll = new JScrollPane(consoleArea);
		topSrcoll.setBorder(null);
		top.add(topSrcoll);
	}

	private void addElementToToolbar(JPanel toolbar) {
		returnDirLb = addIconLable(FTPIcon.getReturnIcon());
		returnDirLb.addMouseListener(this);
		toolbar.add(returnDirLb);
		toDirTf = new MyTextField(10, 10, 50, 30);
		toDirTf.setText("/test");
		toolbar.add(toDirTf);
		toDirLb = addIconLable(FTPIcon.getGotoicon());
		toDirLb.addMouseListener(this);
		toolbar.add(toDirLb);
		newfolderLb = addIconLable(FTPIcon.getNewfoldericon());
		newfolderLb.addMouseListener(this);
		toolbar.add(newfolderLb);
		downloadLb = addIconLable(FTPIcon.getDownloadicon());
		downloadLb.addMouseListener(this);
		toolbar.add(downloadLb);
		uploadLb = addIconLable(FTPIcon.getUploadicon());
		uploadLb.addMouseListener(this);
		toolbar.add(uploadLb);
		deleteFileLb = addIconLable(FTPIcon.getDeleteicon());
		deleteFileLb.addMouseListener(this);
		toolbar.add(deleteFileLb);
		refreshLb = addIconLable(FTPIcon.getRefreshicon());
		refreshLb.addMouseListener(this);
		toolbar.add(refreshLb);

	}

	protected void refresh(String dir) {
		addTreeToEast();
		treeScroll.setViewportView(tree);
		east.repaint();
		addTableModel(dir);
		tableChanged(new TableModelEvent(remoteTable.getModel()));
		tableChanged(new TableModelEvent(serverResponseTable.getModel()));
	}

	private JLabel addIconLable(String location) {
		JLabel label = new MyLabel(10, 10, "", 15, Color.BLACK);
		ImageIcon icon = new ImageIcon(location);
		label.setIcon(icon);
		return label;
	}

	private void addTableModel(String dir) {
		String[] columnNames = { "ID", "Name", "Type", "Size(Bytes)", "Date" };
		FTPFile[] files = null;
		try {
			files = util.listFile(ftpClient, dir);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Object[][] data = new Object[files.length][columnNames.length];

		int index = 0;
		for (FTPFile file : files) {
			data[index] = getData(file, index);
			index++;
		}
		fileModel = new MyTableModel(columnNames, data);
	}

	private String[] getData(FTPFile file, int index) {
		DateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String[] data = new String[5];
		data[0] = String.valueOf(index);
		data[1] = file.getName();

		if (file.getType() == 1) {
			data[2] = "folder";
		} else {
			data[2] = "file";
		}
		data[3] = String.valueOf(file.getSize());
		data[4] = dateFormater.format(file.getTimestamp().getTime()).toString();
		return data;
	}

	private void addElementToBottom(JPanel bottom) {
		String[] columnNames = { "ID", "Status" };
		String[] rep = util.getResponse(ftpClient);
		Object[][] data = new Object[rep.length][columnNames.length];
		
		for (int i = 0; i < rep.length; i++) {
			data[i][0] = String.valueOf(i);
			data[i][1] = rep[i];
		}
		responseModel = new MyTableModel(columnNames, data);
	}

	private JPanel createPanel(int width, int height, Color color, String title, Color textColor) {
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(width, height));
		panel.setBackground(color);
		TitledBorder border = BorderFactory.createTitledBorder(null, title, TitledBorder.CENTER, TitledBorder.CENTER,
				new Font("Arial", Font.BOLD, 13), textColor);
		panel.setBorder(border);
		return panel;
	}

	public class CreateChildNodes implements Runnable {

		private DefaultMutableTreeNode root;

		private File fileRoot;

		public CreateChildNodes(File fileRoot, DefaultMutableTreeNode root) {
			this.fileRoot = fileRoot;
			this.root = root;
		}

		@Override
		public void run() {
			createChildren(fileRoot, root);
		}

		private void createChildren(File fileRoot, DefaultMutableTreeNode node) {
			File[] files = fileRoot.listFiles();
			if (files == null)
				return;

			for (File file : files) {
				DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(new FileNode(file));
				node.add(childNode);
				if (file.isDirectory()) {
					createChildren(file, childNode);
				}
			}
		}

	}

	public class FileNode {

		private File file;

		public FileNode(File file) {
			this.file = file;
		}

		@Override
		public String toString() {
			String name = file.getName();
			if (name.equals("")) {
				return file.getAbsolutePath();
			} else {
				return name;
			}
		}
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		if(e.getSource()==remoteTable.getModel()) {
			remoteTable = new JTable(fileModel);
			tablescrool.setViewportView(remoteTable);
			tablepane.repaint();
		}if(e.getSource()==serverResponseTable.getModel()) {
			serverResponseTable = new JTable(responseModel);
			responseScroll.setViewportView(serverResponseTable);
			bottom.repaint();
		}
			
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == returnDirLb) {
			if (util.changeDirectory(ftpClient, toDirTf.getText())) {
				refresh("/");
				toDirTf.setText("/");
			}
		}
		if (e.getSource() == toDirLb) {
			if (util.changeDirectory(ftpClient, toDirTf.getText())) {
				refresh(toDirTf.getText());
			}
		}
		if(e.getSource() == downloadLb) {
			SwingFileDownloadFTP frame = new SwingFileDownloadFTP();
			frame.setVisible(true);
		}
		if(e.getSource() == uploadLb) {
			SwingFileUploadFTP frame = new SwingFileUploadFTP(util, ftpClient);
			frame.setVisible(true);
		}
		if(e.getSource()==refreshLb) {
			refresh(toDirTf.getText());
		}
		if(e.getSource()==deleteFileLb) {
			SwingFileDeleteFTP frame = new SwingFileDeleteFTP(ftpClient);
			frame.setVisible(true);
		}
		if(e.getSource()==newfolderLb) {
			SwingCreateDirectory frame = new SwingCreateDirectory(ftpClient);
			frame.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}