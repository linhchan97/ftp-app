package doanmang.ftp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 * A utility class that provides functionality for working with files in a FTP server
 * server.
 * 
 * 
 */
public class FTPUtility {

	private String host;
	private int port;
	private String username;
	private String password;

	private FTPClient ftpClient = new FTPClient();
	private int replyCode;

	private OutputStream outputStream;
	private InputStream inputStream;
	
	public FTPUtility() {
		
	}
	public FTPUtility(String host, int port, String user, String pass) {
		this.host = host;
		this.port = port;
		this.username = user;
		this.password = pass;
	}

	/**
	 * Connect and login to the server.
	 * 
	 * @throws FTPException
	 */
	public boolean connect() throws FTPException {
		try {
			ftpClient.connect(host, port);
			replyCode = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode)) {
				return false;
//				throw new FTPException("FTP serve refused connection.");
				
			}

			boolean logged = ftpClient.login(username, password);
			
			if (!logged) {
				// failed to login
				ftpClient.disconnect();
				return false;
//				throw new FTPException("Could not login to the server.");
				
			}

			ftpClient.enterLocalPassiveMode();
			return logged;
		} catch (IOException ex) {
			return false;
//			throw new FTPException("I/O error: " + ex.getMessage());
		}
	}

	/**
	 * Start uploading a file to the server
	 * @param uploadFile the file to be uploaded
	 * @param destDir destination directory on the server 
	 * where the file is stored
	 * @throws FTPException if client-server communication error occurred
	 */
	public void uploadFile(File uploadFile, String destDir) throws FTPException {
		try {
			boolean success = ftpClient.changeWorkingDirectory(destDir);
			if (!success) {
				throw new FTPException("Could not change working directory to "
						+ destDir + ". The directory may not exist.");
			}
			
			success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);			
			if (!success) {
				throw new FTPException("Could not set binary file type.");
			}
			
			outputStream = ftpClient.storeFileStream(uploadFile.getName());
			
		} catch (IOException ex) {
			throw new FTPException("Error uploading file: " + ex.getMessage());
		}
	}

	/**
	 * Write an array of bytes to the output stream.
	 */
	public void writeFileBytes(byte[] bytes, int offset, int length)
			throws IOException {
		outputStream.write(bytes, offset, length);
	}
	
	/**
	 * Complete the upload operation.
	 */
	public void finishOS() throws IOException {
		outputStream.close();
		ftpClient.completePendingCommand();
	}
	/**
	 * Complete the download operation.
	 */
	public void finishIS() throws IOException {
		inputStream.close();
		ftpClient.completePendingCommand();
	}
	/**
	 * Gets size (in bytes) of the file on the server.
	 * 
	 * @param filePath
	 *            Path of the file on server
	 * @return file size in bytes
	 * @throws FTPException
	 */

	public long getFileSize(String filePath) throws FTPException {
		try {
			FTPFile file = ftpClient.mlistFile(filePath);
			if (file == null) {
				throw new FTPException("The file may not exist on the server!");
			}
			return file.getSize();
		} catch (IOException ex) {
			throw new FTPException("Could not determine size of the file: "
					+ ex.getMessage());
		}
	}
	/**
	 * Log out and disconnect from the server
	 */
	public void disconnect() throws FTPException {
		if (ftpClient.isConnected()) {
			try {
				if (!ftpClient.logout()) {
					throw new FTPException("Could not log out from the server");
				}
				ftpClient.disconnect();
			} catch (IOException ex) {
				throw new FTPException("Error disconnect from the server: "
						+ ex.getMessage());
			}
		}
	}
	
	public boolean deleteFileFTP(String filePath, FTPClient ftpClient) {
		try {
			return ftpClient.deleteFile(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public String[] getResponse(FTPClient ftpClient) {
		return ftpClient.getReplyStrings();
	}

	public FTPFile[] listFile(FTPClient ftpClient, String dir) throws IOException {
		return  ftpClient.listFiles(dir);
	}
	public FTPClient getClient() {
		return ftpClient;
	}

	public boolean changeDirectory(FTPClient ftpClient, String dirName) {
		try {
			return ftpClient.changeWorkingDirectory(dirName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Start downloading a file from the server
	 * 
	 * @param downloadPath
	 *            Full path of the file on the server
	 * @throws FTPException
	 *             if client-server communication error occurred
	 */
	public void downloadFile(String downloadPath) throws FTPException {
		try {

			boolean success = ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			if (!success) {
				throw new FTPException("Could not set binary file type.");
			}

			inputStream = ftpClient.retrieveFileStream(downloadPath);

			if (inputStream == null) {
				throw new FTPException("Could not open input stream. The file may not exist on the server.");
			}
		} catch (IOException ex) {
			throw new FTPException("Error downloading file: " + ex.getMessage());
		}
	}
	/**
	 * Return InputStream of the remote file on the server.
	 */
	public InputStream getInputStream() {
		return inputStream;
	}
	public boolean createDirectory(String text, FTPClient ftpClient) {
		try {
			return ftpClient.makeDirectory(text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


}