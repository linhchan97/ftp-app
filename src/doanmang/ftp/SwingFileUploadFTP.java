package doanmang.ftp;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.apache.commons.net.ftp.FTPClient;

import doanhdh.lib.FTPInfor;
import doanmang.lib.JFilePicker;

/**
 * A Swing application that uploads files to a FTP server.
 * 
 * @author www.codejava.net
 *
 */
public class SwingFileUploadFTP extends JFrame implements PropertyChangeListener {

	private JLabel labelUploadPath = new JLabel("Upload path:");

	private JTextField fieldUploadPath = new JTextField(30);

	private JFilePicker filePicker = new JFilePicker("Choose file: ", "Browse");

	private JButton buttonUpload = new JButton("Upload");

	private JLabel labelProgress = new JLabel("Progress:");
	private JProgressBar progressBar = new JProgressBar(0, 100);
	private FTPInfor info;
	private FTPUtility util;
	private FTPClient ftpClient;

	public SwingFileUploadFTP(FTPUtility util, FTPClient ftpClient) {
		super("Swing File Upload to FTP server");
		this.util = util;
		this.ftpClient = ftpClient;
		// set up layout
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(3, 3, 3, 3);

		// set up components
		filePicker.setMode(JFilePicker.MODE_OPEN);

		buttonUpload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					buttonUploadActionPerformed(event);
				} catch (FTPException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		progressBar.setPreferredSize(new Dimension(200, 20));
		progressBar.setStringPainted(true);

		constraints.gridx = 0;
		constraints.gridy = 0;
		add(labelUploadPath, constraints);

		constraints.gridx = 1;
		add(fieldUploadPath, constraints);

		constraints.gridx = 0;
		constraints.gridwidth = 0;
		constraints.gridy = 1;
		constraints.anchor = GridBagConstraints.WEST;

		add(filePicker, constraints);

		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.NONE;
		add(buttonUpload, constraints);

		constraints.gridx = 0;
		constraints.gridy = 5;
		constraints.gridwidth = 1;
		constraints.anchor = GridBagConstraints.WEST;
		add(labelProgress, constraints);

		constraints.gridx = 1;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(progressBar, constraints);
		pack();
		setLocationRelativeTo(null);
	}
	/**
	 * handle click event of the Upload button
	 */
	private void buttonUploadActionPerformed(ActionEvent event) throws FTPException {
		String uploadPath = fieldUploadPath.getText();
		String filePath = filePicker.getSelectedFilePath();
		File uploadFile = new File(filePath);
		progressBar.setValue(0);
		UploadTask task = new UploadTask("localhost",2121,"admin","admin", uploadPath,
				uploadFile);
		task.addPropertyChangeListener(this);
		task.execute();
	}

	/**
	 * Update the progress bar's state whenever the progress of upload changes.
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
		}
	}

}