package doanmang.ftp;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginForm extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JLabel hostLabel;
	private  JLabel userLabel;
	private  JLabel passwordLabel;
	private  JTextField hostText;
	private  JTextField userText;
	private  JPasswordField passwordText;
	private  JButton loginButton;
	
	

	public LoginForm() {
		this.setTitle("Login to FTP server");
		this.setSize(300, 200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		this.add(panel);
		placeComponents(panel);
		
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new LoginForm();
	}

		
	private void placeComponents(JPanel panel) {
		
		panel.setLayout(null);

		hostLabel = new JLabel("Host");
		hostLabel.setBounds(10, 10, 80, 25);
		panel.add(hostLabel);

		JTextField hostText = new JTextField(20);
		hostText.setBounds(100, 10, 160, 25);
		hostText.setText("localhost");
		panel.add(hostText);

		userLabel = new JLabel("Username");
		userLabel.setBounds(10, 40, 80, 25);
		panel.add(userLabel);

		JTextField userText = new JTextField(20);
		userText.setBounds(100, 40, 160, 25);
		userText.setText("admin");
		panel.add(userText);

		passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 70, 80, 25);
		panel.add(passwordLabel);

		passwordText = new JPasswordField(20);
		passwordText.setBounds(100, 70, 160, 25);
		passwordText.setText("admin");
		panel.add(passwordText);

		loginButton = new JButton("login");
		loginButton.setBounds(100, 110, 80, 25);
		loginButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String host = hostText.getText();
				String username = userText.getText();
				String password = new String(passwordText.getPassword());

				FTPUtility util = new FTPUtility(host, 2121, username, password);
			try {
				boolean login = util.connect();
				if(login) {
					JOptionPane.showMessageDialog(null,"Login successfully!");
					exitCurrent();
					JFrame frame = new MainUI(util, util.getClient());
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(null, "Server is not working!");
				}
			} catch (HeadlessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (FTPException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
		});
		panel.add(loginButton);
		
	}

	protected void exitCurrent() {
		this.setVisible(false);
		
	}

}
