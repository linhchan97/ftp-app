package doanmang.ftp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.net.ftp.FTPClient;

public class SwingFileDeleteFTP extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel filePath;
	private JTextField filePathTf;
	private JButton deleteBt;
	private FTPClient ftpClient;
	
	public SwingFileDeleteFTP(FTPClient ftpClient) {
		this.ftpClient = ftpClient;
		this.setTitle("Delete file FTP server");
		JPanel panel = new JPanel();
		this.add(panel);
		this.setSize(500, 100);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
		filePath = new JLabel("File path:");
		filePath.setBounds(5,20,50,50);
		filePathTf = new JTextField(20);
		deleteBt = new JButton("Delete");
		deleteBt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int dialogConfirm = JOptionPane.showConfirmDialog(deleteBt, "Do you want to delete this file?");
				if(JOptionPane.YES_OPTION == dialogConfirm) {
					FTPUtility util = new FTPUtility();
					boolean result = util.deleteFileFTP(filePathTf.getText(),ftpClient);
					if(result) {
						JOptionPane.showMessageDialog(null, "Deleted file successfully!");
						
						exitFrame();
					}else {
						JOptionPane.showMessageDialog(null, "Can not delete this file! This file may not exist!");
					}
				}
			}
		});
		panel.add(filePath);
		panel.add(filePathTf);
		panel.add(deleteBt);
		this.setVisible(true);
	}
	protected void exitFrame() {
		this.setVisible(false);
	}
	
	
}
